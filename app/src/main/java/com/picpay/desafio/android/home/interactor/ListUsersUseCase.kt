package com.picpay.desafio.android.home.interactor

import com.picpay.desafio.android.common.Resource
import com.picpay.desafio.android.common.UseCase
import com.picpay.desafio.android.home.model.User

abstract class ListUsersUseCase : UseCase<Unit, Resource<List<User>>>()
package com.picpay.desafio.android.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.picpay.desafio.android.common.Resource
import com.picpay.desafio.android.home.interactor.ListUsersUseCase
import com.picpay.desafio.android.home.model.User
import kotlinx.coroutines.launch

class MainViewModel(
    private val listUseCase: ListUsersUseCase

) : ViewModel() {
    private var _listUsers: MutableLiveData<Resource<List<User>>> = MutableLiveData()
    val users: LiveData<Resource<List<User>>> get() = _listUsers

    init {
        viewModelScope.launch {
            fetchUsers()
        }
    }

    suspend fun fetchUsers() {
        _listUsers.postValue(Resource.loading())
        _listUsers.postValue(listUseCase.execute(Unit))
    }

}

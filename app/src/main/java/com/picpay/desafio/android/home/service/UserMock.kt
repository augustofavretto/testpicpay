package com.picpay.desafio.android.home.service

object UserMock {
    const val USER_NAME = "Eduardo Santo"
    const val USER_AVATAR = "https://randomuser.me/api/portraits/men/9.jpg"
    const val USER_ID = 1001
    const val USER_USER_NAME= "@eduardo.santos"
}
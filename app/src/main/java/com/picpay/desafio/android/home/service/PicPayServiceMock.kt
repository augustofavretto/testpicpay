package com.picpay.desafio.android.home.service

import com.picpay.desafio.android.home.model.User
import com.picpay.desafio.android.home.service.UserMock.USER_AVATAR
import com.picpay.desafio.android.home.service.UserMock.USER_ID
import com.picpay.desafio.android.home.service.UserMock.USER_NAME
import com.picpay.desafio.android.home.service.UserMock.USER_USER_NAME
import retrofit2.Response

class PicPayServiceMock : PicPayService {

    override suspend fun getUsers(): Response<List<User>> {
        return Response.success(listOf(
            User(USER_AVATAR,USER_NAME,USER_ID,USER_USER_NAME)
        ))
    }
}
package com.picpay.desafio.android.di

import com.picpay.desafio.android.home.service.PicPayService
import com.picpay.desafio.android.home.MainViewModel
import com.picpay.desafio.android.home.interactor.ListUsersCaseImpl
import com.picpay.desafio.android.home.interactor.ListUsersUseCase
import com.picpay.desafio.android.home.service.PicPayServiceMock
import com.picpay.desafio.android.utils.resolveRetrofit
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val remoteModule = module {
    single { provideOkHttpClient() }
    single { provideRetrofit(get()) }
}

val servicesModule = module {
    factory<PicPayService> {
        resolveRetrofit() ?: PicPayServiceMock()
    }

}

val uiModule = module {
    viewModel { MainViewModel( get()) }
}

val repositoryModule = module {
    factory<ListUsersUseCase> { ListUsersCaseImpl(get()) }
}




package com.picpay.desafio.android.home.interactor

import android.content.Context
import com.picpay.desafio.android.common.Resource
import com.picpay.desafio.android.home.model.User
import com.picpay.desafio.android.home.service.PicPayService

class ListUsersCaseImpl(
    private val service: PicPayService
) : ListUsersUseCase() {

    override suspend fun execute(params: Unit): Resource<List<User>> {
        return try {
            val response = service.getUsers()
            when {
                response.isSuccessful -> {
                    response.body().let {
                        Resource.success(it)
                    }
                }
                else -> Resource.error(Exception(response.message()))
            }

        } catch (e: Exception) {
            Resource.error(e)
        }
    }
}
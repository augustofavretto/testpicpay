package com.picpay.desafio.android.home

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.picpay.desafio.android.R
import com.picpay.desafio.android.common.Resource
import com.picpay.desafio.android.home.adapter.UserListAdapter
import com.picpay.desafio.android.utils.Util
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(), CoroutineScope by MainScope() {
    private lateinit var adapter: UserListAdapter
    private val viewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupObservables()
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        uiRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            this@MainActivity.adapter = UserListAdapter()
            adapter = this@MainActivity.adapter
        }
    }

    private fun setupObservables() {
        viewModel.users.observe(this, Observer { resource ->
            when (resource?.status) {
                Resource.Status.SUCCESS -> {
                    uiProgressBar.visibility = View.GONE
                    resource.data?.let { listUsers -> adapter.users = listUsers }
                }
                Resource.Status.LOADING -> {
                    uiProgressBar.visibility = View.VISIBLE
                }
                Resource.Status.ERROR -> {
                    Util.showShortToastMessage(this, getString(R.string.error))
                    uiProgressBar.visibility = View.GONE
                }
                else -> {
                }
            }
        })
    }
}

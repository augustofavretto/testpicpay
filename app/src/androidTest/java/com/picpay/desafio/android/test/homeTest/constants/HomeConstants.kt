package com.picpay.desafio.android.test.homeTest.constants

import com.picpay.desafio.android.home.service.UserMock


object HomeConstants {
    const val USER_NAME = UserMock.USER_NAME
}
package com.picpay.desafio.android.test.homeTest.robot

import android.content.Context
import androidx.test.rule.ActivityTestRule
import com.picpay.desafio.android.R
import com.picpay.desafio.android.home.MainActivity
import com.picpay.desafio.android.test.common.ScreenRobot
import com.picpay.desafio.android.test.homeTest.constants.HomeConstants
import okhttp3.mockwebserver.MockWebServer


class RobotHome {

    private val robot = ScreenRobot()
    private val server = MockWebServer()

    fun launchLoginScreen(testRule: ActivityTestRule<MainActivity>) {
        testRule.launchActivity(null)
    }

    fun shouldDisplayTitle(context:Context) {
        robot.checkIsDisplayed(VIEW_TITLE)
        robot.checkViewContainsText(context.getString(R.string.title))
    }

    fun shouldDisplayListItem(){
        robot.checkViewContainsText(HomeConstants.USER_NAME)
    }

    companion object {
        private val VIEW_TITLE = R.id.title
        private val VIEW_LIST = R.id.uiRecyclerView
    }
}
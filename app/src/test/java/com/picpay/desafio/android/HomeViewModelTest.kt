package com.picpay.desafio.android

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.picpay.desafio.android.common.Resource
import com.picpay.desafio.android.di.remoteModule
import com.picpay.desafio.android.di.repositoryModule
import com.picpay.desafio.android.di.servicesModule
import com.picpay.desafio.android.di.uiModule
import com.picpay.desafio.android.home.MainViewModel
import com.picpay.desafio.android.home.interactor.ListUsersUseCase
import com.picpay.desafio.android.home.model.User
import com.picpay.desafio.android.home.service.UserMock
import io.mockk.mockk
import io.mockk.verify
import junit.framework.Assert.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.AutoCloseKoinTest
import org.koin.test.get
import org.koin.test.mock.declare
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class HomeViewModelTest: AutoCloseKoinTest() {

    private fun viewModel(): MainViewModel {
        val viewModel = MainViewModel(get())
        viewModel.users.observeForever(onDataLoadedObserver)
        return viewModel
    }
    private val onDataLoadedObserver = mockk<Observer<Resource<List<User>>>>(relaxed = true)
    private val listUsersUseCase: ListUsersUseCase = Mockito.mock(ListUsersUseCase::class.java)
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @get:Rule
    val rule = InstantTaskExecutorRule()


    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    init {
        startKoin {  listOf(remoteModule, repositoryModule, uiModule, servicesModule) }
        declare {
            single { listUsersUseCase }
        }
    }

    @After
    fun tearDown(){
        stopKoin()
    }

    @Test
    fun verifyIfIsNullList() {
        val viewModel = viewModel()
        runBlocking {
            `when` (listUsersUseCase.execute(Unit)).thenReturn(null)
            viewModel.fetchUsers()
            assertNull("Should cleared list", viewModel.users.value)
        }
    }

    @Test
    fun verifyIfIValidList() {
        val viewModel = viewModel()
        val mockList = mockList()
        runBlocking {
            `when` (listUsersUseCase.execute(Unit)).thenReturn(mockList)
            viewModel.fetchUsers()
            assertNotNull("Should have persisted current list",viewModel.fetchUsers())
        }
    }

    @Test
    fun verifyIfObserverReceiveValue() {
        val viewModel = viewModel()
        val mockList = mockList()
        runBlocking {
            `when` (listUsersUseCase.execute(Unit)).thenReturn(mockList)
            viewModel.fetchUsers()
            verify { onDataLoadedObserver.onChanged( mockList ) }
        }
    }

    private fun mockList() = Resource.success(
        listOf(
            User(
                UserMock.USER_AVATAR,
                UserMock.USER_NAME,
                UserMock.USER_ID,
                UserMock.USER_USER_NAME
            )
        )
    )
}